import svar
import numpy as np
import cv2
import argparse

rtmv=svar.load('svar_rtmv')

parser = argparse.ArgumentParser()
parser.add_argument("-rtmv",default="/data/zhaoyong/Datasets/Xinda/1/20210315_153352.rtmv",help="The rtmv file")
  
args = parser.parse_args()

options={"ip":args.rtmv,"port":1212,"skip":1}

dataset=rtmv.DatasetNetwork(options)

while True:
  fr=dataset.grabFrame()
  if fr == None:
    break

  print(fr.id(),fr.timestamp(),fr.getImage(0,0),fr.getHeight2Ground(),fr.getGPSLLA(),fr.getPitchYawRoll())

  image=fr.getImage(0,0)
  
  m= np.array(image)
  m.dtype='uint8'
  print(m.shape,m.dtype.name) # BGRA

  cv2.imshow('video', m)
  k=cv2.waitKey(10)
  if k == 27:
    exit(0)


